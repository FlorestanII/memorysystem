package de.johannespollitt.memorysystem;

import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class ArrayUtils {
	
	public static float[] combineFloatArrays(float[]... arrays) {
		int totalLength = 0;
		for (float[] array : arrays) {
			totalLength += array.length;
		}
		float[] result = new float[totalLength];
		int pointer = 0;
		for (float[] array : arrays) {
			for (float f : array) {
				result[pointer++] = f;
			}
		}
		return result;
	}
	
	public static float[] combineFloatArrays(List<float[]> arrays) {
		int totalLength = 0;
		for (float[] array : arrays) {
			totalLength += array.length;
		}
		float[] result = new float[totalLength];
		int pointer = 0;
		for (float[] array : arrays) {
			for (float f : array) {
				result[pointer++] = f;
			}
		}
		return result;
	}
	
	public static int[] combineIntArrays(int[]... arrays) {
		int totalLength = 0;
		for (int[] array : arrays) {
			totalLength += array.length;
		}
		int[] result = new int[totalLength];
		int pointer = 0;
		for (int[] array : arrays) {
			for (int i : array) {
				result[pointer++] = i;
			}
		}
		return result;
	}
	
	public static int[] combineIntArrays(List<int[]> arrays) {
		int totalLength = 0;
		for (int[] array : arrays) {
			totalLength += array.length;
		}
		int[] result = new int[totalLength];
		int pointer = 0;
		for (int[] array : arrays) {
			for (int i : array) {
				result[pointer++] = i;
			}
		}
		return result;
	}
	
	public static byte[] combineByteArrays(byte[]... arrays) {
		int totalLength = 0;
		for (byte[] array : arrays) {
			totalLength += array.length;
		}
		byte[] result = new byte[totalLength];
		int pointer = 0;
		for (byte[] array : arrays) {
			for (byte b : array) {
				result[pointer++] = b;
			}
		}
		return result;
	}
	
	public static byte[] combineByteArrays(List<byte[]> arrays) {
		int totalLength = 0;
		for (byte[] array : arrays) {
			totalLength += array.length;
		}
		byte[] result = new byte[totalLength];
		int pointer = 0;
		for (byte[] array : arrays) {
			for (byte b : array) {
				result[pointer++] = b;
			}
		}
		return result;
	}
}
