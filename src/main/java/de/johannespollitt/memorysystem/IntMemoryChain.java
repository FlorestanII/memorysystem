package de.johannespollitt.memorysystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public abstract class IntMemoryChain {
	
	private final int maxSize;
	
	private IntMemorySlot start;
	private IntMemorySlot end;
	
	private final List<IntMemorySlot> gaps = new ArrayList<IntMemorySlot>();
	
	private int endPointer = 0;
	
	public IntMemoryChain() {
		this(0);
	}
	
	public IntMemoryChain(int maxSize) {
		this.maxSize = maxSize;
	}
	
	public IntMemorySlot allocateMemory(int size) {
		
		for (IntMemorySlot gap : this.gaps) {
			if (gap.getLength() >= size) {
				IntMemorySlot slot = new IntMemorySlot(this, gap.getStart(), size, false);
				
				IntMemorySlot.insertInGap(slot, gap, this.gaps);
				
				if (gap.getPrevious() == null) {
					this.start = slot;
				}

				if (gap.getNext() == null && gap.getLength() == size) {
					this.end = slot;
				}
				
				return slot;
			}
		}
		
		if (this.endPointer + size <= maxSize || maxSize == 0) {
			IntMemorySlot slot = new IntMemorySlot(this, this.endPointer, size, false);
			
			if (this.start == null) {
				this.start = slot;
			}
			
			IntMemorySlot.append(this.end, slot);
			
			this.endPointer += size;
			this.end = slot;
			
			return slot;
		}
		
		return null;
	}
	
	public IntMemorySlot allocateMemory(int[] data) {
		
		IntMemorySlot slot = allocateMemory(data.length);
		
		if (slot != null) {
			slot.setData(data);
		}
		
		return slot;
	}
	
	public boolean refactor() {
		
		if (this.gaps.isEmpty()) {
			return false;
		}
		
		IntMemorySlot gap = this.gaps.remove(0);
		
		gap.getNext().connectToPrevious(gap.getPrevious());
		IntMemorySlot current = gap.getNext();
		
		if (gap.getPrevious() == null) {
			this.start = gap.getNext();
		}
		
		List<int[]> data = new ArrayList<int[]>();
		
		while (current != null && !current.isGap()) {
			current.shiftLeft(gap.getLength());
			data.add(current.getData());
			current = current.getNext();
		}
		
		onStore(ArrayUtils.combineIntArrays(data), gap.getStart());
		
		if (current == null) {
			this.endPointer -= gap.getLength();
		} else {
			current.increaseStart(-gap.getLength());
		}
		
		return true;
	}
	
	public void free(IntMemorySlot slot) {
		if (slot.getNext() == null) {
			freeNextToEnd(slot);
		} else if (slot.getNext().isGap) {
			freeNextToGap(slot);
		} else {
			freeNextToData(slot);
		}
	}
	
	private void freeNextToEnd(IntMemorySlot slot) {
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			if (slot.getPrevious().getPrevious() != null) {
				slot.getPrevious().getPrevious().connectToNext(null);
			} else {
				this.start = null;
			}
			
			this.gaps.remove(slot.getPrevious());
			
			this.end = slot.getPrevious().getPrevious();
			
			this.endPointer -= slot.getLength() + slot.getPrevious().getLength();
			
		} else {
			
			if (slot.getPrevious() != null) {
				slot.getPrevious().connectToNext(null);
			} else {
				this.start = null;
			}
			
			this.end = slot.getPrevious();
			this.endPointer -= slot.getLength();
		}
	}
	
	private void freeNextToGap(IntMemorySlot slot) {
		
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			slot.getPrevious().increaseLength(slot.getLength() + slot.getNext().getLength());
			slot.getPrevious().connectToNext(slot.getNext().getNext());
			this.gaps.remove(slot.getNext());
			
		} else {
			
			if (slot.getPrevious() == null) {
				this.start = slot.getNext();
			}
			
			slot.getNext().increaseStart(-slot.getLength());
			slot.getNext().connectToPrevious(slot.getPrevious());
			
		}
		
	}
	
	private void freeNextToData(IntMemorySlot slot) {
		
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			slot.getPrevious().connectToNext(slot.getNext());
			slot.getPrevious().increaseLength(slot.getLength());
		
		} else {
		
			IntMemorySlot gap = new IntMemorySlot(this, slot.getStart(), slot.getLength(), true);
			
			this.gaps.add(gap);
			
			if (slot.getPrevious() == null) {
				this.start = gap;
			}
			
			gap.connectToPrevious(slot.getPrevious());
			gap.connectToNext(slot.getNext());	
		
		}
			
	}
	
	public int getEndPointer() {
		return this.endPointer;
	}

	public IntMemorySlot getStart() {
		return this.start;
	}
	
	public IntMemorySlot getEnd() {
		return this.end;
	}
	
	public List<IntMemorySlot> getSlots() {
		if (start != null) {
			return start.getSlotList();
		}
		return null;
	}
	
	public int getMaxSize() {
		return this.maxSize;
	}
	
	public abstract void onStore(int[] data, int start);
	
}
