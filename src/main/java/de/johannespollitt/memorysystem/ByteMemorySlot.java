package de.johannespollitt.memorysystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 *         Copyright (C) 2019 Johannes Pollitt All rights reserved
 */

public class ByteMemorySlot {

	protected final ByteMemoryChain chain;

	protected int start;
	protected int end;

	protected boolean isGap;

	protected ByteMemorySlot previous;
	protected ByteMemorySlot next;

	protected byte[] data;

	public ByteMemorySlot(ByteMemoryChain chain, int start, int length, boolean isGap) {
		this.chain = chain;
		this.start = start;
		this.end = start + length;
		this.isGap = isGap;
	}

	public int getStart() {
		return this.start;
	}

	public int getEnd() {
		return this.end;
	}

	public int getLength() {
		return this.end - this.start;
	}

	public void shiftLeft(int amount) {
		this.start -= amount;
		this.end -= amount;
	}

	public void increaseStart(int amount) {
		this.start += amount;
	}

	public void increaseLength(int amount) {
		this.end += amount;
	}

	public boolean isGap() {
		return this.isGap;
	}

	public byte[] getData() {
		return this.data;
	}

	public boolean setData(byte[] data) {
		if (data.length == getLength()) {
			this.data = data;
			this.chain.onStore(data, this.start);
			return true;
		}
		return false;
	}

	public ByteMemorySlot getPrevious() {
		return this.previous;
	}

	public ByteMemorySlot getNext() {
		return this.next;
	}

	public void connectToPrevious(ByteMemorySlot slot) {
		this.previous = slot;

		if (slot != null) {
			slot.next = this;
		}
	}

	public void connectToNext(ByteMemorySlot slot) {
		this.next = slot;

		if (slot != null) {
			slot.previous = this;
		}
	}

	protected List<ByteMemorySlot> generateSlotList(List<ByteMemorySlot> list) {
		list.add(this);

		if (this.next != null) {
			this.next.generateSlotList(list);
		}

		return list;
	}

	public List<ByteMemorySlot> getSlotList() {
		return generateSlotList(new ArrayList<ByteMemorySlot>());
	}

	public ByteMemoryChain getChain() {
		return this.chain;
	}

	public static void append(ByteMemorySlot currentEnd, ByteMemorySlot newEnd) {
		if (currentEnd != null) {
			currentEnd.connectToNext(newEnd);
		} else if (newEnd != null) {
			newEnd.connectToPrevious(currentEnd);
		}
	}

	public static void insertInGap(ByteMemorySlot slot, ByteMemorySlot gap, List<ByteMemorySlot> gaps) {
		if (gap.getPrevious() != null) {
			gap.getPrevious().connectToNext(slot);
		}

		if (gap.getLength() == slot.getLength()) {
			slot.connectToNext(gap.getNext());
			gaps.remove(gap);
		} else if (gap.getLength() > slot.getLength()) {
			slot.connectToNext(gap);
			gap.increaseStart(slot.getLength());
		}
	}

}
