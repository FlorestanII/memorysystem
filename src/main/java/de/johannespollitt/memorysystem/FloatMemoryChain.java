package de.johannespollitt.memorysystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public abstract class FloatMemoryChain {
	
	private final int maxSize;
	
	private FloatMemorySlot start;
	private FloatMemorySlot end;
	
	private final List<FloatMemorySlot> gaps = new ArrayList<FloatMemorySlot>();
	
	private int endPointer = 0;
	
	public FloatMemoryChain() {
		this(0);
	}
	
	public FloatMemoryChain(int maxSize) {
		this.maxSize = maxSize;
	}
	
	public FloatMemorySlot allocateMemory(int size) {
		
		for (FloatMemorySlot gap : this.gaps) {
			if (gap.getLength() >= size) {
				FloatMemorySlot slot = new FloatMemorySlot(this, gap.getStart(), size, false);
				
				FloatMemorySlot.insertInGap(slot, gap, this.gaps);
				
				if (gap.getPrevious() == null) {
					this.start = slot;
				}

				if (gap.getNext() == null && gap.getLength() == size) {
					this.end = slot;
				}
				
				return slot;
			}
		}
		
		if (this.endPointer + size <= maxSize || maxSize == 0) {
			FloatMemorySlot slot = new FloatMemorySlot(this, this.endPointer, size, false);
			
			if (this.start == null) {
				this.start = slot;
			}
			
			FloatMemorySlot.append(this.end, slot);
			
			this.endPointer += size;
			this.end = slot;
			
			return slot;
		}
		
		return null;
	}
	
	public FloatMemorySlot allocateMemory(float[] data) {
		
		FloatMemorySlot slot = allocateMemory(data.length);
		
		if (slot != null) {
			slot.setData(data);
		}
		
		return slot;
	}
	
	public boolean refactor() {
		
		if (this.gaps.isEmpty()) {
			return false;
		}
		
		FloatMemorySlot gap = this.gaps.remove(0);
		
		gap.getNext().connectToPrevious(gap.getPrevious());
		FloatMemorySlot current = gap.getNext();
		
		if (gap.getPrevious() == null) {
			this.start = gap.getNext();
		}
		
		List<float[]> data = new ArrayList<float[]>();
		
		while (current != null && !current.isGap()) {
			current.shiftLeft(gap.getLength());
			data.add(current.getData());
			current = current.getNext();
		}
		
		onStore(ArrayUtils.combineFloatArrays(data), gap.getStart());
		
		if (current == null) {
			this.endPointer -= gap.getLength();
		} else {
			current.increaseStart(-gap.getLength());
		}
		
		return true;
	}
	
	public void free(FloatMemorySlot slot) {
		if (slot.getNext() == null) {
			freeNextToEnd(slot);
		} else if (slot.getNext().isGap) {
			freeNextToGap(slot);
		} else {
			freeNextToData(slot);
		}
	}
	
	private void freeNextToEnd(FloatMemorySlot slot) {
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			if (slot.getPrevious().getPrevious() != null) {
				slot.getPrevious().getPrevious().connectToNext(null);
			} else {
				this.start = null;
			}
			
			this.gaps.remove(slot.getPrevious());
			
			this.end = slot.getPrevious().getPrevious();
			
			this.endPointer -= slot.getLength() + slot.getPrevious().getLength();
			
		} else {
			
			if (slot.getPrevious() != null) {
				slot.getPrevious().connectToNext(null);
			} else {
				this.start = null;
			}
			
			this.end = slot.getPrevious();
			this.endPointer -= slot.getLength();
		}
	}
	
	private void freeNextToGap(FloatMemorySlot slot) {
		
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			slot.getPrevious().increaseLength(slot.getLength() + slot.getNext().getLength());
			slot.getPrevious().connectToNext(slot.getNext().getNext());
			this.gaps.remove(slot.getNext());
			
		} else {
			
			if (slot.getPrevious() == null) {
				this.start = slot.getNext();
			}
			
			slot.getNext().increaseStart(-slot.getLength());
			slot.getNext().connectToPrevious(slot.getPrevious());
			
		}
		
	}
	
	private void freeNextToData(FloatMemorySlot slot) {
		
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			slot.getPrevious().connectToNext(slot.getNext());
			slot.getPrevious().increaseLength(slot.getLength());
		
		} else {
		
			FloatMemorySlot gap = new FloatMemorySlot(this, slot.getStart(), slot.getLength(), true);
			
			this.gaps.add(gap);
			
			if (slot.getPrevious() == null) {
				this.start = gap;
			}
			
			gap.connectToPrevious(slot.getPrevious());
			gap.connectToNext(slot.getNext());	
		
		}
			
	}
	
	public int getEndPointer() {
		return this.endPointer;
	}

	public FloatMemorySlot getStart() {
		return this.start;
	}
	
	public FloatMemorySlot getEnd() {
		return this.end;
	}
	
	public List<FloatMemorySlot> getSlots() {
		if (start != null) {
			return start.getSlotList();
		}
		return null;
	}
	
	public int getMaxSize() {
		return this.maxSize;
	}
	
	public abstract void onStore(float[] data, int start);
	
}
