package de.johannespollitt.memorysystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class IntMemorySlot {
	
	protected final IntMemoryChain chain;
	
	protected int start;
	protected int end;
	
	protected boolean isGap;
	
	protected IntMemorySlot previous;
	protected IntMemorySlot next;
	
	protected int[] data;
	
	public IntMemorySlot(IntMemoryChain chain, int start, int length, boolean isGap) {
		this.chain = chain;
		this.start = start;
		this.end = start + length;
		this.isGap = isGap;
	}
	
	public int getStart() {
		return this.start;
	}
	
	public int getEnd() {
		return this.end;
	}
	
	public int getLength() {
		return this.end-this.start;
	}
	
	public void shiftLeft(int amount) {
		this.start -= amount;
		this.end -= amount;
	}
	
	public void increaseStart(int amount) {
		this.start += amount;
	}
	
	public void increaseLength(int amount) {
		this.end += amount;
	}
	
	public boolean isGap() {
		return this.isGap;
	}
	
	public int[] getData() {
		return this.data;
	}
	
	public boolean setData(int[] data) {
		if (data.length == getLength()) {
			this.data = data;
			this.chain.onStore(data, this.start);
			return true;
		}
		return false;
	}
	
	public IntMemorySlot getPrevious() {
		return this.previous;
	}
	
	public IntMemorySlot getNext() {
		return this.next;
	}
	
	public void connectToPrevious(IntMemorySlot slot) {
		this.previous = slot;
		
		if (slot != null) {
			slot.next = this;
		}
	}
	
	public void connectToNext(IntMemorySlot slot) {
		this.next = slot;
		
		if (slot != null) {
			slot.previous = this;
		}
	}
	
	protected List<IntMemorySlot> generateSlotList(List<IntMemorySlot> list) {
		list.add(this);
		
		if (this.next != null) {
			this.next.generateSlotList(list);
		}
		
		return list;
	}
	
	public List<IntMemorySlot> getSlotList() {
		return generateSlotList(new ArrayList<IntMemorySlot>());
	}
	
	public IntMemoryChain getChain(){
		return this.chain;
	}
	
	public static void append(IntMemorySlot currentEnd, IntMemorySlot newEnd) {
		if (currentEnd != null) {
			currentEnd.connectToNext(newEnd);
		} else if (newEnd != null) {
			newEnd.connectToPrevious(currentEnd);
		}
	}
	
	public static void insertInGap(IntMemorySlot slot, IntMemorySlot gap, List<IntMemorySlot> gaps) {
		if (gap.getPrevious() != null) {
			gap.getPrevious().connectToNext(slot);
		}
		
		if (gap.getLength() == slot.getLength()) {
			slot.connectToNext(gap.getNext());
			gaps.remove(gap);
		} else if (gap.getLength() > slot.getLength()) {
			slot.connectToNext(gap);
			gap.increaseStart(slot.getLength());
		}
	}
	
}
