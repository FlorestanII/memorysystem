package de.johannespollitt.memorysystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public class FloatMemorySlot {
	
	protected final FloatMemoryChain chain;
	
	protected int start;
	protected int end;
	
	protected boolean isGap;
	
	protected FloatMemorySlot previous;
	protected FloatMemorySlot next;
	
	protected float[] data;
	
	public FloatMemorySlot(FloatMemoryChain chain, int start, int length, boolean isGap) {
		this.chain = chain;
		this.start = start;
		this.end = start + length;
		this.isGap = isGap;
	}
	
	public int getStart() {
		return this.start;
	}
	
	public int getEnd() {
		return this.end;
	}
	
	public int getLength() {
		return this.end-this.start;
	}
	
	public void shiftLeft(int amount) {
		this.start -= amount;
		this.end -= amount;
	}
	
	public void increaseStart(int amount) {
		this.start += amount;
	}
	
	public void increaseLength(int amount) {
		this.end += amount;
	}
	
	public boolean isGap() {
		return this.isGap;
	}
	
	public float[] getData() {
		return this.data;
	}
	
	public boolean setData(float[] data) {
		if (data.length == getLength()) {
			this.data = data;
			this.chain.onStore(data, this.start);
			return true;
		}
		return false;
	}
	
	public FloatMemorySlot getPrevious() {
		return this.previous;
	}
	
	public FloatMemorySlot getNext() {
		return this.next;
	}
	
	public void connectToPrevious(FloatMemorySlot slot) {
		this.previous = slot;
		
		if (slot != null) {
			slot.next = this;
		}
	}
	
	public void connectToNext(FloatMemorySlot slot) {
		this.next = slot;
		
		if (slot != null) {
			slot.previous = this;
		}
	}
	
	protected List<FloatMemorySlot> generateSlotList(List<FloatMemorySlot> list) {
		list.add(this);
		
		if (this.next != null) {
			this.next.generateSlotList(list);
		}
		
		return list;
	}
	
	public List<FloatMemorySlot> getSlotList() {
		return generateSlotList(new ArrayList<FloatMemorySlot>());
	}
	
	public FloatMemoryChain getChain(){
		return this.chain;
	}
	
	public static void append(FloatMemorySlot currentEnd, FloatMemorySlot newEnd) {
		if (currentEnd != null) {
			currentEnd.connectToNext(newEnd);
		} else if (newEnd != null) {
			newEnd.connectToPrevious(currentEnd);
		}
	}
	
	public static void insertInGap(FloatMemorySlot slot, FloatMemorySlot gap, List<FloatMemorySlot> gaps) {
		if (gap.getPrevious() != null) {
			gap.getPrevious().connectToNext(slot);
		}
		
		if (gap.getLength() == slot.getLength()) {
			slot.connectToNext(gap.getNext());
			gaps.remove(gap);
		} else if (gap.getLength() > slot.getLength()) {
			slot.connectToNext(gap);
			gap.increaseStart(slot.getLength());
		}
	}
	
}
