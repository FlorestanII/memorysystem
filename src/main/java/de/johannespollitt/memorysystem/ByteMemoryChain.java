package de.johannespollitt.memorysystem;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

public abstract class ByteMemoryChain {
	
	private final int maxSize;
	
	private ByteMemorySlot start;
	private ByteMemorySlot end;
	
	private final List<ByteMemorySlot> gaps = new ArrayList<ByteMemorySlot>();
	
	private int endPointer = 0;
	
	public ByteMemoryChain() {
		this(0);
	}
	
	public ByteMemoryChain(int maxSize) {
		this.maxSize = maxSize;
	}
	
	public ByteMemorySlot allocateMemory(int size) {
		
		for (ByteMemorySlot gap : this.gaps) {
			if (gap.getLength() >= size) {
				ByteMemorySlot slot = new ByteMemorySlot(this, gap.getStart(), size, false);
				
				ByteMemorySlot.insertInGap(slot, gap, this.gaps);
				
				if (gap.getPrevious() == null) {
					this.start = slot;
				}

				if (gap.getNext() == null && gap.getLength() == size) {
					this.end = slot;
				}
				
				return slot;
			}
		}
		
		if (this.endPointer + size <= maxSize || maxSize == 0) {
			ByteMemorySlot slot = new ByteMemorySlot(this, this.endPointer, size, false);
			
			if (this.start == null) {
				this.start = slot;
			}
			
			ByteMemorySlot.append(this.end, slot);
			
			this.endPointer += size;
			this.end = slot;
			
			return slot;
		}
		
		return null;
	}
	
	public ByteMemorySlot allocateMemory(byte[] data) {
		
		ByteMemorySlot slot = allocateMemory(data.length);
		
		if (slot != null) {
			slot.setData(data);
		}
		
		return slot;
	}
	
	public boolean refactor() {
		
		if (this.gaps.isEmpty()) {
			return false;
		}
		
		ByteMemorySlot gap = this.gaps.remove(0);
		
		gap.getNext().connectToPrevious(gap.getPrevious());
		ByteMemorySlot current = gap.getNext();
		
		if (gap.getPrevious() == null) {
			this.start = gap.getNext();
		}
		
		List<byte[]> data = new ArrayList<byte[]>();
		
		while (current != null && !current.isGap()) {
			current.shiftLeft(gap.getLength());
			data.add(current.getData());
			current = current.getNext();
		}
		
		onStore(ArrayUtils.combineByteArrays(data), gap.getStart());
		
		if (current == null) {
			this.endPointer -= gap.getLength();
		} else {
			current.increaseStart(-gap.getLength());
		}
		
		return true;
	}
	
	public void free(ByteMemorySlot slot) {
		if (slot.getNext() == null) {
			freeNextToEnd(slot);
		} else if (slot.getNext().isGap) {
			freeNextToGap(slot);
		} else {
			freeNextToData(slot);
		}
	}
	
	private void freeNextToEnd(ByteMemorySlot slot) {
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			if (slot.getPrevious().getPrevious() != null) {
				slot.getPrevious().getPrevious().connectToNext(null);
			} else {
				this.start = null;
			}
			
			this.gaps.remove(slot.getPrevious());
			
			this.end = slot.getPrevious().getPrevious();
			
			this.endPointer -= slot.getLength() + slot.getPrevious().getLength();
			
		} else {
			
			if (slot.getPrevious() != null) {
				slot.getPrevious().connectToNext(null);
			} else {
				this.start = null;
			}
			
			this.end = slot.getPrevious();
			this.endPointer -= slot.getLength();
		}
	}
	
	private void freeNextToGap(ByteMemorySlot slot) {
		
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			slot.getPrevious().increaseLength(slot.getLength() + slot.getNext().getLength());
			slot.getPrevious().connectToNext(slot.getNext().getNext());
			this.gaps.remove(slot.getNext());
			
		} else {
			
			if (slot.getPrevious() == null) {
				this.start = slot.getNext();
			}
			
			slot.getNext().increaseStart(-slot.getLength());
			slot.getNext().connectToPrevious(slot.getPrevious());
			
		}
		
	}
	
	private void freeNextToData(ByteMemorySlot slot) {
		
		if (slot.getPrevious() != null && slot.getPrevious().isGap()) {
			
			slot.getPrevious().connectToNext(slot.getNext());
			slot.getPrevious().increaseLength(slot.getLength());
		
		} else {
		
			ByteMemorySlot gap = new ByteMemorySlot(this, slot.getStart(), slot.getLength(), true);
			
			this.gaps.add(gap);
			
			if (slot.getPrevious() == null) {
				this.start = gap;
			}
			
			gap.connectToPrevious(slot.getPrevious());
			gap.connectToNext(slot.getNext());	
		
		}
			
	}
	
	public int getEndPointer() {
		return this.endPointer;
	}

	public ByteMemorySlot getStart() {
		return this.start;
	}
	
	public ByteMemorySlot getEnd() {
		return this.end;
	}
	
	public List<ByteMemorySlot> getSlots() {
		if (start != null) {
			return start.getSlotList();
		}
		return null;
	}
	
	public int getMaxSize() {
		return this.maxSize;
	}
	
	public abstract void onStore(byte[] data, int start);
	
}
