package de.johannespollitt.memorysystem.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.johannespollitt.memorysystem.FloatMemoryChain;
import de.johannespollitt.memorysystem.FloatMemorySlot;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

class MemoryChainTest {

	@Test
	void testMemoryChain() {
		
		FloatMemoryChain chain = new FloatMemoryChain() {
			
			@Override
			public void onStore(float[] data, int start) {
			
			}
		};
		
		chain.allocateMemory(new float[] {0f});
		FloatMemorySlot slot2 = chain.allocateMemory(new float[] {1f});
		FloatMemorySlot slot3 = chain.allocateMemory(new float[] {2f});
		chain.allocateMemory(new float[] {3f});
		
		String msg = "";
		
		for (FloatMemorySlot slot : chain.getSlots()) {
			for (float f : slot.getData()) {
				msg += f + " ";
			}
		}
		
		assertEquals("0.0 1.0 2.0 3.0 ", msg);
		
		chain.free(slot2);
		
		msg = "";
		
		for (FloatMemorySlot slot : chain.getSlots()) {
			if (slot.getData() != null) {
				for (float f : slot.getData()) {
					msg += f + " ";
				}
			}
		}
		
		assertEquals("0.0 2.0 3.0 ", msg);
		
		assertEquals(4, chain.getEndPointer());
		
		assertEquals(true, chain.refactor());
		
		assertEquals(3, chain.getEndPointer());
		
		msg = "";
		
		for (FloatMemorySlot slot : chain.getSlots()) {
			if (slot.getData() != null) {
				for (float f : slot.getData()) {
					msg += f + " ";
				}
			}
		}
		
		assertEquals("0.0 2.0 3.0 ", msg);
		
		chain.free(slot3);
		
		chain.allocateMemory(new float[] {4f});
		
		msg = "";
		
		for (FloatMemorySlot slot : chain.getSlots()) {
			if (slot.getData() != null) {
				for (float f : slot.getData()) {
					msg += f + " ";
				}
			}
		}
		
		assertEquals("0.0 4.0 3.0 ", msg);
		assertEquals(3, chain.getEndPointer());
		
	}

}
