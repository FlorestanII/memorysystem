package de.johannespollitt.memorysystem.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.johannespollitt.memorysystem.FloatMemorySlot;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

class MemorySlotConnectionSlot {

	@Test
	void slotConnection() {
		
		FloatMemorySlot slot1 = new FloatMemorySlot(null, 0, 10, false);
		FloatMemorySlot slot2 = new FloatMemorySlot(null, 10, 10, false);
		FloatMemorySlot slot3 = new FloatMemorySlot(null, 20, 10, false);
		FloatMemorySlot slot4 = new FloatMemorySlot(null, 30, 40, false);
		
		slot1.connectToNext(slot2);
		slot3.connectToPrevious(slot2);
		FloatMemorySlot.append(slot3, slot4);
		
		List<FloatMemorySlot> list = slot1.getSlotList();
		
		assertEquals(4, list.size());
		
	}

}
