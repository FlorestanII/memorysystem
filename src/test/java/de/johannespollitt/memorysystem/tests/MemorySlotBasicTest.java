package de.johannespollitt.memorysystem.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.johannespollitt.memorysystem.FloatMemorySlot;

/**
 * @author Johannes Pollitt
 * 
 * Copyright (C) 2019 Johannes Pollitt
 * All rights reserved
 */

class MemorySlotBasicTest {

	@Test
	void contruction() {
		FloatMemorySlot slot = new FloatMemorySlot(null, 5, 10, false);
		
		assertEquals(false, slot.isGap());
		
		assertEquals(5, slot.getStart());
		assertEquals(15, slot.getEnd());
		assertEquals(10, slot.getLength());
		
		slot.shiftLeft(5);
		
		assertEquals(0, slot.getStart());
		assertEquals(10, slot.getEnd());
		assertEquals(10, slot.getLength());
		
		slot.increaseStart(5);
		
		assertEquals(5, slot.getStart());
		assertEquals(10, slot.getEnd());
		assertEquals(5, slot.getLength());
		
	}

}
